package observability

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/gin-contrib/logger"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
)

func NewLoggerWithMetrics() gin.HandlerFunc {
	withLogger := logger.WithLogger(func(c *gin.Context, out io.Writer, latency time.Duration) zerolog.Logger {
		// Metrics for prometheus
		responseTimeIngress.WithLabelValues(c.Request.Method, c.Request.URL.Path).Observe(latency.Seconds())
		status := c.Writer.Status()
		// Logging output
		return zerolog.New(out).With().
			Str("origin", "handlers").
			Str("time", time.Now().Local().String()).
			Str("method", c.Request.Method).
			Str("response", fmt.Sprintf("%d", status)).
			Str("path", c.Request.URL.Path).
			Str("user-agent", c.Request.UserAgent()).
			Str("ip", c.ClientIP()).
			Dur("latency", latency).
			Logger()
	})
	return logger.SetLogger(withLogger)
}

// Info logs for cron -> stdout
func NewCronLogger() zerolog.Logger {
	return zerolog.New(os.Stdout).With().
		Str("origin", "cron").
		Str("time", time.Now().Local().String()).
		Logger()
}

// Error logs for cron -> stderr
func NewCronErrorLogger() zerolog.Logger {
	logger := zerolog.New(os.Stderr).With().
		Str("origin", "cron").
		Str("time", time.Now().Local().String()).
		Logger()

	return logger
}

// Error logs for api -> stderr
func NewApiErrorLogger() zerolog.Logger {

	logger := zerolog.New(os.Stderr).With().
		Str("origin", "api").
		Str("time", time.Now().Local().String()).
		Logger()
	return logger
}
