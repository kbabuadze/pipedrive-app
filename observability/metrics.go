package observability

import (
	"fmt"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	// Response timeout for the incomming requests
	responseTimeIngress = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "pipedrive_response_time_ingress",
		Help: "Response Time Ingress",
	}, []string{"method", "url"})

	// Response timeout for the outgoing requests
	responseTimeEgress = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "pipedrive_response_time_egress",
		Help: "Response Time Egress",
	}, []string{"method", "url"})
)

// MetricsRoundTripper measures how much time has elapsed since requets has been made
type MetricsRoundTripper struct {
	Proxied http.RoundTripper
}

func (mrt *MetricsRoundTripper) RoundTrip(req *http.Request) (res *http.Response, err error) {
	start := time.Now()
	res, err = mrt.Proxied.RoundTrip(req)
	if err != nil {
		fmt.Printf("Error: %v", err)
	}

	// construct url to avoid leaking api key
	url := req.URL.Scheme + "://" + req.URL.Host + req.URL.Path

	responseTimeEgress.WithLabelValues(req.Method, url).Observe(float64(time.Since(start).Seconds()))
	return

}
