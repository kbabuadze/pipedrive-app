package cron

import (
	"context"
	"net/http"
	"time"

	"github.com/google/go-github/v40/github"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/robfig/cron/v3"

	"gitlab.com/kbabuadze/pipedrive-app/config"
	"gitlab.com/kbabuadze/pipedrive-app/observability"
	"gitlab.com/kbabuadze/pipedrive-app/repository/mysqlrepo"
	"gitlab.com/kbabuadze/pipedrive-app/services"
	"golang.org/x/oauth2"
)

// Setup new logger for cron jobs
var (
	cronLogger      = observability.NewCronLogger()
	cronErrorLogger = observability.NewCronErrorLogger()
	startTime       time.Time
)

func Init(config *config.PipeConfig) {

	startTime = time.Now()

	// Setup config
	mysqlConf := config.MySQLParams

	dbc, err := sqlx.Connect("mysql", mysqlConf.Username+":"+mysqlConf.Password+"@tcp("+mysqlConf.Host+":"+mysqlConf.Port+")/"+mysqlConf.Database+"?parseTime=true")
	if err != nil {
		cronErrorLogger.Error().Msgf("Couldn't connect to the database%+v", errors.Errorf("%s", ""))
	}

	err = dbc.Ping()
	if err != nil {
		cronErrorLogger.Error().Msgf("%+v", errors.Errorf("%s", err))
	}

	// Setup github.Client with PAT
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: config.GitHubParams.ApiToken},
	)
	tc := oauth2.NewClient(context.Background(), ts)

	tc.Transport = &observability.MetricsRoundTripper{
		Proxied: tc.Transport,
	}

	client := github.NewClient(tc)

	// Equip http client with metrics
	clientWithMetrics := http.Client{
		Transport: &observability.MetricsRoundTripper{
			Proxied: http.DefaultTransport,
		},
	}

	// Setup sync
	sync := *services.NewSyncService(mysqlrepo.NewMysqlSyncRepo(dbc))

	// Setup UserHandlers
	userSvc := *services.NewUsersService(mysqlrepo.NewMysqlUserRepo(dbc))
	gistSvc := *services.NewGistservice(mysqlrepo.NewMysqlGistsRepo(dbc), client, &clientWithMetrics)
	pipedriveSvc := *services.NewPipedriveService(clientWithMetrics, config.PipedriveParams.ApiToken, config.PipedriveParams.ApiURL)

	gistsJob := NewGistJob(userSvc, gistSvc, pipedriveSvc, sync)

	c := cron.New(cron.WithChain(
		cron.Recover(cron.DefaultLogger),
	))

	if _, err := c.AddFunc(config.CronParams.CronInterval, gistsJob.ProcessGists); err != nil {
		cronErrorLogger.Error().Msgf("Error schuduling job: %+v", errors.Errorf("%s", err))
	}

	c.Start()
}
