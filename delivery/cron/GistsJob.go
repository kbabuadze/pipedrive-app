package cron

import (
	"math/rand"
	"time"

	"gitlab.com/kbabuadze/pipedrive-app/domain"
	"gitlab.com/kbabuadze/pipedrive-app/services"
)

type GistJob struct {
	userSvc      services.UserService
	gistSvc      services.GistService
	pipedriveSvc services.PipederiveService
	sync         services.SyncService
}

func NewGistJob(
	userSvc services.UserService,
	gistSvc services.GistService,
	pipedriveSvc services.PipederiveService,
	sync services.SyncService,
) *GistJob {
	return &GistJob{
		userSvc:      userSvc,
		gistSvc:      gistSvc,
		pipedriveSvc: pipedriveSvc,
		sync:         sync,
	}
}

// ProcessGists job checks all users in database,
// for each user it checks details.json file in user's gists
// and creates deal/activity from the json provided
func (j *GistJob) ProcessGists() {

	// Sleep for random interval avoid two hosts running cron at the same time
	rand.Seed(time.Now().UnixNano() + startTime.Unix())
	// We don't really need crypto grade random
	/* #nosec */
	r := rand.Intn(1000) + 1000
	cronLogger.Info().Msgf("Sleep for %v milliseconds to better avoid duplicate runs", r)
	time.Sleep(time.Duration(r) * time.Millisecond)

	syncStruct := domain.Sync{}

	processing, err := j.sync.GetFlag()

	if err != nil {
		cronErrorLogger.Error().Stack().Err(err).Msg("Error getting sync status")
		return
	}

	if processing {
		cronLogger.Info().Msg("Processing has already started by another instance")
		return
	}
	syncStruct.Processing = true
	if err := j.sync.SetFlag(&syncStruct); err != nil {
		cronErrorLogger.Error().Stack().Err(err).Msg("Error setting sync status")
		return
	}

	cronLogger.Info().Msg("Started processing gists")
	// List all users
	users, err := j.userSvc.ListAll()
	if err != nil {
		cronErrorLogger.Error().Stack().Err(err).Msg("Error getting users")
		return
	}

	cronLogger.Info().Msgf("Found %v user(s)", len(users))

	// For each user get gist
	for i := range users {

		user := domain.User{
			ID: users[i].ID,
		}
		cronLogger.Info().Msgf("Processing gists for user with ID: %v", users[i].ID)

		gists, err := j.gistSvc.ListByID(&user)
		if err != nil {
			cronErrorLogger.Error().Stack().Err(err).Msg("Error getting gist url")
		}

		cronLogger.Info().Msgf("Found %v new gists", len(gists))

		// For each gist find if file "details.json" exist
		for _, gist := range gists {

			// Parse details.json
			detailsGist, err := j.gistSvc.GetDetails(gist)

			if err == domain.ErrorGistDetailsJsonNotFound {
				cronLogger.Info().Msg("Couldn't find details JSON in this gist")
				continue
			}

			if err != nil {
				cronErrorLogger.Error().Stack().Err(err).Msg("Error getting details.json")
				continue
			}

			detailsGist.UserID = users[i].ID
			detailsGist.ID = *gist.ID

			// Create deal in pipedrive
			deal, err := j.pipedriveSvc.CreateDeal(&domain.PipedriveDeal{
				Title:    detailsGist.Deal.Title,
				Value:    detailsGist.Deal.Value,
				Currency: detailsGist.Deal.Currency,
			})

			if err != nil {
				cronErrorLogger.Error().Stack().Err(err).Msg("Error creating a deal")
				continue
			}

			if !deal.Success {
				cronErrorLogger.Error().Msgf("%v %v", deal.Error, deal.ErrorInfo)
				continue
			}

			// Create record in repository to avoid duplicate entires
			err = j.gistSvc.Create(users[i].ID, domain.Gist{
				ID: *gist.ID,
			})

			if err != nil {
				cronErrorLogger.Error().Stack().Err(err).Msg("Error creating gist record in repository")
				continue
			}

			// Create activities
			j.pipedriveSvc.CreateActivities(detailsGist.Activities, deal.Data.ID)
		}

		if err := j.userSvc.UpdateLastScan(&user); err != nil {
			cronErrorLogger.Error().Stack().Err(err).Msg("Updating user last_scan")
			continue
		}
	}

	syncStruct.Processing = false
	if err := j.sync.SetFlag(&syncStruct); err != nil {
		cronErrorLogger.Error().Stack().Err(err).Msg("Error setting sync status")
		return
	}
	cronLogger.Info().Msg("Finished processing gists")
}
