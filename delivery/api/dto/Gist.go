package dto

type Gist struct {
	ID          string `json:"id"`
	URL         string `json:"url"`
	CreatedAt   string `json:"created_at"`
	Description string `json:"description"`
}
