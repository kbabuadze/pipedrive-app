package dto

type User struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	LastLogin string `json:"last_login"`
	LastScan  string `json:"last_scan"`
}
