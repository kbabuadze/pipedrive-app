package api

import (
	"net/http"

	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
)

type HealthHandler struct {
	db    *sqlx.DB
	store redis.Store
}

func NewHealthHandler(db *sqlx.DB, store redis.Store) *HealthHandler {
	return &HealthHandler{
		db:    db,
		store: store,
	}
}

// Simple healthcheck for API
func (hh *HealthHandler) Health(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "OK"})
}

// Healthcheck for cron if mysql
func (hh *HealthHandler) Ready(c *gin.Context) {
	err := hh.db.Ping()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "NOK"})
		apiErrorLogger.Error().Stack().Err(err).Msg("")
	}

	c.JSON(http.StatusOK, gin.H{"message": "OK"})
}
