package api

import (
	"net/http"
	"net/url"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
	"gitlab.com/kbabuadze/pipedrive-app/services"
	"golang.org/x/crypto/bcrypt"
)

type UserHandler struct {
	service services.UserService
}

func NewUserHandler(service services.UserService) *UserHandler {
	return &UserHandler{
		service: service,
	}
}

func (uh *UserHandler) Create(c *gin.Context) {

	user := domain.User{}

	err := c.ShouldBindJSON(&user)

	if user.Name == "" {
		c.AbortWithStatusJSON(400, gin.H{"message": "Please provide username"})
		return
	}

	if err != nil {
		apiErrorLogger.Error().Stack().Err(err).Msg("Error while binding user")
		c.AbortWithStatusJSON(500, gin.H{"message": "Unexpected error"})
		return
	}

	err = uh.service.Create(&user)
	if err != nil {
		apiErrorLogger.Error().Stack().Err(err).Msg("Error while creating user")
		c.AbortWithStatusJSON(500, gin.H{"message": "Unexpected error"})
		return
	}

	c.JSON(200, gin.H{"message": "user added"})
}

// Return Login Form
func (uh *UserHandler) Auth(c *gin.Context) {
	sessions := sessions.Default(c)
	loggedIn := sessions.Get("loggedIn")

	if loggedIn == "true" {
		location := url.URL{Path: "/"}
		c.Redirect(http.StatusFound, location.RequestURI())
		c.Abort()
		return
	}

	c.HTML(http.StatusOK, "login.html", "")
}

// Handle user login
func (uh *UserHandler) Login(c *gin.Context) {

	username := c.PostForm("username")
	password := c.PostForm("password")

	user, err := uh.service.Login(username, password)

	// If password doesn't match or user not found
	if err == bcrypt.ErrMismatchedHashAndPassword || err == bcrypt.ErrHashTooShort {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "user/password not found"})
		return
	}

	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Unexpected error"})
		apiErrorLogger.Error().Stack().Err(err).Msg("")
		return
	}

	sessions := sessions.Default(c)

	sessions.Set("loggedIn", "true")
	sessions.Set("userID", user.ID)
	sessions.Set("username", user.Name)
	sessions.Set("lastLogin", user.LastLogin.Time.Unix())

	// Update user's last login in repository
	err = uh.service.Update(user)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Unexpected error"})
		apiErrorLogger.Error().Stack().Err(err).Msg("Error updating user")
		return
	}

	// Save user's last login in session store
	err = sessions.Save()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Unexpected error"})
		apiErrorLogger.Error().Stack().Err(errors.Errorf("%s", err)).Msg("Error while saving the session")
		return
	}
	location := url.URL{Path: "/"}
	c.Redirect(http.StatusFound, location.RequestURI())

}

// Logout user
func (uh *UserHandler) Logout(c *gin.Context) {
	session := sessions.Default(c)
	session.Clear()
	session.Options(sessions.Options{Path: "/", MaxAge: -1})
	if err := session.Save(); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Unexpected error"})
		apiErrorLogger.Error().Stack().Err(errors.Errorf("%s", err)).Msg("Error while saving the session")
		return
	}
	location := url.URL{Path: "/"}
	c.Redirect(http.StatusFound, location.RequestURI())
}

// List all users
func (uh *UserHandler) ListAll(c *gin.Context) {

	users, err := uh.service.ListAll()
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"message": "Unexpected error"})
		apiErrorLogger.Error().Stack().Err(err).Msg("Error while listing users")
		return
	}

	c.JSON(200, gin.H{"users": users})
}

// Return RegisterForm
func (uh *UserHandler) RegisterView(c *gin.Context) {
	c.HTML(http.StatusOK, "register.html", "")
}

// Register User
func (uh *UserHandler) Register(c *gin.Context) {
	username := c.PostForm("username")
	password1 := c.PostForm("password1")
	password2 := c.PostForm("password2")

	if len(username) < 4 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "username should be a list 4 chars"})
		return
	}

	user, err := uh.service.GetByName(username)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Unexpected error"})
		apiErrorLogger.Error().Stack().Err(err).Msg("Error validating user")
		return
	}

	if username == user.Name {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"message": "user already exists in db"})
		return
	}

	if password1 != password2 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "passwords do not match"})
		return
	}

	bytes, err := bcrypt.GenerateFromPassword([]byte(password1), 14)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Unexpected error"})
		apiErrorLogger.Error().Stack().Err(err).Msg("Error encrypting password")
	}

	user.Name = username
	user.Password = string(bytes)

	if err := uh.service.Create(user); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Unexpected error"})
		apiErrorLogger.Error().Stack().Err(err).Msg("Error creating user")
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "User created, please login at /login"})

}
