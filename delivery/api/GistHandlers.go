package api

import (
	"net/http"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/kbabuadze/pipedrive-app/delivery/api/dto"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
	"gitlab.com/kbabuadze/pipedrive-app/services"
)

type GistHandler struct {
	service services.GistService
}

func NewGistHandler(service services.GistService) *GistHandler {
	return &GistHandler{
		service: service,
	}
}

// Get new gists since last visit
func (gh *GistHandler) GetLatest(c *gin.Context) {
	session := sessions.Default(c)
	userID := session.Get("userID").(int)
	username := session.Get("username").(string)

	lastLogin := time.Unix(session.Get("lastLogin").(int64), 0)

	gistsResponse := make([]dto.Gist, 0)

	gists, err := gh.service.ListLatest(userID, lastLogin)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "Unexpected Error"})
		apiErrorLogger.Error().Stack().Err(err).Msg("Error listing latests gists")
		return
	}

	for _, gist := range gists {
		gistsResponse = append(gistsResponse, gh.convert(*gist, username))
	}

	if len(gists) == 0 {
		c.JSON(http.StatusOK, gin.H{"message": "no new gists were added since your last visit"})
		return
	}
	c.JSON(http.StatusOK, gistsResponse)
}

// List all the gists for loggedin user
func (gh *GistHandler) ListAll(c *gin.Context) {
	session := sessions.Default(c)
	userID := session.Get("userID").(int)
	username := session.Get("username").(string)

	gistsResponse := make([]dto.Gist, 0)

	gists, err := gh.service.ListLatest(userID, time.Time{})

	for _, gist := range gists {
		gistsResponse = append(gistsResponse, gh.convert(*gist, username))
	}

	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "Unexpected Error"})
		apiErrorLogger.Error().Stack().Err(err).Msg("")
		return
	}
	if len(gists) == 0 {
		c.JSON(http.StatusOK, gin.H{"message": "no new gists were added since your last visit"})
		return
	}
	c.JSON(http.StatusOK, gistsResponse)
}

func (gh *GistHandler) convert(gist domain.Gist, username string) dto.Gist {
	t := time.Unix(gist.CreatedAt, 0)
	return dto.Gist{
		ID:        gist.ID,
		URL:       "https://gist.github.com/" + username + "/" + gist.ID,
		CreatedAt: t.Format("02/01/2006, 15:04:05"),
	}
}
