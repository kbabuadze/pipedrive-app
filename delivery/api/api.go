package api

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
	_ "github.com/go-sql-driver/mysql"
	"github.com/google/go-github/v40/github"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/gin-gonic/gin"
	"gitlab.com/kbabuadze/pipedrive-app/config"
	"gitlab.com/kbabuadze/pipedrive-app/delivery/api/dto"
	"gitlab.com/kbabuadze/pipedrive-app/observability"
	"gitlab.com/kbabuadze/pipedrive-app/repository/mysqlrepo"

	"gitlab.com/kbabuadze/pipedrive-app/services"
)

var PipeRouter *gin.Engine

var (
	apiErrorLogger = observability.NewApiErrorLogger()
)

func Init(config *config.PipeConfig) {

	router := gin.New()
	// Setup Logging
	router.Use(observability.NewLoggerWithMetrics())
	router.LoadHTMLGlob("delivery/api/templates/*")

	// Setup session auth

	redisConf := config.RedisParams
	// Connect to redis
	store, err := redis.NewStore(100, "tcp", redisConf.Address, redisConf.Password, []byte(redisConf.Key))
	if err != nil {
		log.Printf("Sessions store is down")
	}

	// Setup session store
	store.Options(sessions.Options{
		Domain:   config.SessionParams.Domain,
		MaxAge:   86400,
		Path:     "/",
		Secure:   true,
		HttpOnly: true,
	})

	// Sessions setup
	router.Use(sessions.Sessions("sessions", store))

	private := router.Group("/")
	private.Use(Private)

	// Setup Basic Auth
	authorized := router.Group("/", gin.BasicAuth(gin.Accounts{
		config.MetricsParams.Uername: config.MetricsParams.Password,
	}))

	mysqlConf := config.MySQLParams

	db, err := sqlx.Connect("mysql", mysqlConf.Username+":"+mysqlConf.Password+"@tcp("+mysqlConf.Host+":"+mysqlConf.Port+")/"+mysqlConf.Database+"?parseTime=true")

	if err != nil {
		log.Printf("%+v", errors.Errorf("%s", err))
	}
	// Setup UserHandlers
	userHandler := NewUserHandler(*services.NewUsersService(mysqlrepo.NewMysqlUserRepo(db)))
	gistHandler := NewGistHandler(*services.NewGistservice(mysqlrepo.NewMysqlGistsRepo(db), github.NewClient(http.DefaultClient), &http.Client{}))
	healthHandler := NewHealthHandler(db, store)

	// Health endpotint
	router.GET("/healthz", healthHandler.Health)
	router.GET("/readyz", healthHandler.Ready)

	// Auth routes
	router.GET("/login", userHandler.Auth)
	router.POST("/login", userHandler.Login)
	router.POST("/logout", userHandler.Logout)

	// Registration routes
	router.GET("/register", userHandler.RegisterView)
	router.POST("/register", userHandler.Register)

	// Home
	private.GET("/", func(c *gin.Context) {

		// Get username and lastLogin from session
		session := sessions.Default(c)
		username := session.Get("username").(string)
		lastLogin := session.Get("lastLogin").(int64)

		// Construct User
		user := dto.User{}

		user.Name = username
		user.LastLogin = time.Unix(lastLogin, 0).String()
		if lastLogin == -62135596800 {
			user.LastLogin = "Never"
		}

		// Response
		c.HTML(http.StatusOK, "index.html", gin.H{"user": user})

	})

	// User routes
	private.GET("/users", userHandler.ListAll)
	// Gist routes
	private.GET("/gists", gistHandler.ListAll)
	private.GET("/gists/latest", gistHandler.GetLatest)

	// Setup metrics endpoint
	authorized.GET("/metrics", func(c *gin.Context) {
		handler := promhttp.Handler()
		handler.ServeHTTP(c.Writer, c.Request)
	})

	if err := router.Run(os.Getenv("API_PORT")); err != nil {
		fmt.Printf("%+v", errors.Errorf("%s", err))
	}
}

// Authorization Middleware
func Private(c *gin.Context) {
	session := sessions.Default(c)
	loggedIn := session.Get("loggedIn")
	if loggedIn != "true" {
		// Redirect to login and
		location := url.URL{Path: "/login"}
		c.Redirect(http.StatusFound, location.RequestURI())
		c.Abort()
	}
	// Next handler
	c.Next()
}
