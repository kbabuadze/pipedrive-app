package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"
	"gitlab.com/kbabuadze/pipedrive-app/config"
	"gitlab.com/kbabuadze/pipedrive-app/delivery/api"
	"gitlab.com/kbabuadze/pipedrive-app/delivery/cron"
)

func main() {
	// Enable stacktraces
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack

	pipeConig := config.NewConfig()

	cron.Init(pipeConig)
	api.Init(pipeConig)

}
