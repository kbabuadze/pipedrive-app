CREATE DATABASE IF NOT EXISTS `pipedrive`;

USE `pipedrive`; 

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `last_scan`  TIMESTAMP NULL DEFAULT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `last_login` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=ascii;


INSERT INTO `users` (name,password) VALUES ('cbabuadze','$2a$14$ynRSEH8vbu1zBWcATlfdku8ZBcyF0/iGOLYw3RQJACBfmc02/XqA.');
INSERT INTO `users` (name,password) VALUES ('kbabuadze','$2a$14$0AAZsdGZO0kIuZjJCnORQuMv4nhzqapiQUmnb086q991cK2SzM3uG.');

CREATE TABLE IF NOT EXISTS `gists` (
  `user_id` int(11) NOT NULL,
  `id` varchar(255) NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `user_id_url` (`user_id`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;


CREATE TABLE `sync` (
  `processing` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

INSERT INTO sync(processing) values (false);