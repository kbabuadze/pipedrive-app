#!/bin/sh
echo "Running go mod tidy"
go mod tidy

echo "Building the app"
go build -o /usr/local/bin .

echo "sleep to make sure mysql is up"
sleep 3

echo "Starting the app"
pipedrive-app
