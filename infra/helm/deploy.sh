#!/bin/sh

# exit if any of the commands fails
set -e


# Setup kubernetes client
echo "Downloading kubectl"
curl -LO "https://dl.k8s.io/release/${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"

echo "Downloading checksum"
curl -LO "https://dl.k8s.io/${KUBERNETES_VERSION}/bin/linux/amd64/kubectl.sha256"
openssl sha1 -sha256 kubectl | sha256sum -c

if [ $? -ne 0 ]; then
  exit 1
fi

chmod +x kubectl
mv kubectl  /usr/local/bin/kubectl
mkdir -p ${HOME}/.kube

echo "${KUBE_CONFIG}" | base64 -d >  "${HOME}"/.kube/config
chmod go-r "${HOME}"/.kube/config

# Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh

# Upgrade an app in the cluster

helm upgrade --install pipedrive-app ./infra/helm/pipedrive-app -f infra/helm/pipedrive-app/values.yaml -n "${NAMESPACE}" \
--set image.repository="${CONTAINER_REPO}" \
--set image.tag="${CI_COMMIT_SHORT_SHA}"