# Gist parser app

[![pipeline status](https://gitlab.com/kbabuadze/pipedrive-app/badges/main/pipeline.svg)](https://gitlab.com/kbabuadze/pipedrive-app/-/commits/main)

##### To run the app on you local machine please perform following steps:

```bash
cp .env_exapmle .env
```
Some of the values are already prefiled and setting these three environment variables will get you going

``` 
GIT_API_TOKEN, PIPEDRIVE_API_TOKEN, PIPEDRIVE_API_URL
```
Now you can run:

```bash
docker-compose up --build
```
____
#### After build is finished following ports become available: 

##### App ```localhost:8080```

Users can be registered at [/register](localhost:8080/register), login can be done  at [/login](localhost:8080/register)

**username** has to match **github's username** where gists are located 

##### Prometheus ```localhost:9090```

Prometheus is scraping **App**'s [/metrics](localhost:8080/metrics) endpoint, which is protected with Basic Auth and default credentials are **metrics/metrics**
 
##### Grafana ```localhost:3000```

Grafana can be accessed with **monitoring/monitoring** username and password

____

#### Cron

CronJob internvals are configured in .env `CRON_INTERVAL`.
Job goes over registered users and check if there are any public gists containing **details.json** file, it is parsed and **Deal** is created in Pipedrive.

details.json needs to be in the following format to be successfully picked up by the CronJob
```json
{
  "deal" : {
    "title":"Deal title",
    "value": 100,
    "currency":"EUR"
  },
  "activities":[
    {
      "type":"call"
    }
  ]
}

```



