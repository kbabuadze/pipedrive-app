package domain

import (
	"database/sql"
)

type User struct {
	ID        int          `json:"id" db:"id"`
	Name      string       `json:"name" db:"name"`
	Password  string       `json:"password" db:"password"`
	LastLogin sql.NullTime `json:"last_login" db:"last_login"`
	LastScan  sql.NullTime `json:"last_scan" db:"last_scan"`
	CreatedAt int64        `json:"created_at" db:"created_at"`
}

type UsersRepositroy interface {
	GetByID(id int) (*User, error)
	Create(user *User) error
	ListAll() ([]User, error)
	GetByName(username string) (*User, error)
	UpdateLastLogin(user *User) error
	UpdateLastScan(user *User) error
}
