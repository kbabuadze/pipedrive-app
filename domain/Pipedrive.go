package domain

type PipedriveOrg struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type PipedriveOrgResponse struct {
	Data []PipedriveOrg `json:"data"`
}

type PipedriveActivity struct {
	Type   string `json:"type"`
	DealID int    `json:"deal_id"`
}

type PipedriveActivityResponse struct {
	Success   bool              `json:"success"`
	Data      PipedriveActivity `json:"data"`
	Error     string            `json:"error"`
	ErrorInfo string            `json:"error_info"`
}

type PipedriveDeal struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	Value    int    `json:"value"`
	Currency string `json:"currency"`
}

type PipedriveDealResponse struct {
	Success   bool          `json:"success"`
	Data      PipedriveDeal `json:"data"`
	Error     string        `json:"error"`
	ErrorInfo string        `json:"error_info"`
}
