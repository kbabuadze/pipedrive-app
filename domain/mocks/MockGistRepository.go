package mocks

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
)

type MockGistRepository struct {
	mock.Mock
}

func (m *MockGistRepository) Create(userID int, gist domain.Gist) error { return nil }
func (m *MockGistRepository) ListByUserID(userID int) ([]*domain.Gist, error) {

	gists := []*domain.Gist{
		{
			UserID: 1,
			ID:     "0000000000000000000000000000001",
		},
		{
			UserID: 1,
			ID:     "0000000000000000000000000000002",
		},
	}

	return gists, nil
}
func (m *MockGistRepository) ListLatest(userID int, time int64) ([]*domain.Gist, error) {
	return nil, nil
}
