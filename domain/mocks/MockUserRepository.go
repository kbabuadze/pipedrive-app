package mocks

import (
	"database/sql"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
)

type MockUserRepository struct {
	mock.Mock
}

func (m *MockUserRepository) ListAll() ([]domain.User, error) {

	users := []domain.User{{
		ID:        1,
		Name:      "john",
		LastLogin: sql.NullTime{},
		LastScan:  sql.NullTime{},
		CreatedAt: time.Now().Unix(),
	}, {
		ID:   1,
		Name: "paul",
		LastLogin: sql.NullTime{
			Time: time.Unix(1356048001, 0).UTC(),
		},
		LastScan: sql.NullTime{
			Time: time.Unix(1356048001, 0).UTC(),
		},
		CreatedAt: time.Now().Unix(),
	}}
	return users, nil
}

func (m *MockUserRepository) GetByID(id int) (*domain.User, error)            { return nil, nil }
func (m *MockUserRepository) Create(user *domain.User) error                  { return nil }
func (m *MockUserRepository) GetByName(username string) (*domain.User, error) { return nil, nil }
func (m *MockUserRepository) UpdateLastLogin(user *domain.User) error         { return nil }
func (m *MockUserRepository) UpdateLastScan(user *domain.User) error          { return nil }
