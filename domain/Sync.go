package domain

type Sync struct {
	Processing bool `json:"processing" db:"processing"`
}

type SyncRepository interface {
	SetFlag(sync *Sync) error
	GetFlag() (*Sync, error)
}
