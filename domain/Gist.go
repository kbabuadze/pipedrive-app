package domain

import "errors"

var (
	ErrorGistDetailsJsonNotFound = errors.New("could not find details.json")
)

//Gist simlified struct
type Gist struct {
	UserID      int    `json:"user_id" db:"user_id"`
	Title       string `json:"title" db:"title"`
	Description string `json:"description" db:"description"`
	ID          string `json:"id" db:"id"`
	CreatedAt   int64  `json:"created_at" db:"created_at"`
}

// File with details
type GistDetailsJson struct {
	UserID     int                 `json:"user_id"`
	ID         string              `json:"id"`
	Deal       PipedriveDeal       `json:"deal"`
	Activities []PipedriveActivity `json:"activities"`
}

// Gist repositoty interface
type GistsRepository interface {
	Create(userID int, gist Gist) error
	ListByUserID(userID int) ([]*Gist, error)
	ListLatest(userID int, time int64) ([]*Gist, error)
}
