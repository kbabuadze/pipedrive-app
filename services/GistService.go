package services

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"github.com/google/go-github/v40/github"
	"github.com/pkg/errors"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
)

type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
	Get(url string) (resp *http.Response, err error)
}

type GistService struct {
	repo         domain.GistsRepository
	githubClient *github.Client
	httpClient   HTTPClient
}

// Create New instance of a UserService
func NewGistservice(repo domain.GistsRepository, githubClient *github.Client, httpClient HTTPClient) *GistService {
	return &GistService{
		repo:         repo,
		githubClient: githubClient,
		httpClient:   httpClient,
	}
}

func (g *GistService) Create(userID int, gist domain.Gist) error {
	return g.repo.Create(userID, gist)
}

// Lists gists that haven't been added to the database
func (g *GistService) ListByID(user *domain.User) ([]*github.Gist, error) {
	// Get Gists stored in repository
	gistsFromRepo, err := g.repo.ListByUserID(user.ID)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}

	// Create map for fast comparison
	gistMap := make(map[string]bool)
	for _, gistID := range gistsFromRepo {
		gistMap[gistID.ID] = true
	}

	// Get Gists from github
	gists, _, err := g.githubClient.Gists.List(context.Background(), user.Name, nil)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}

	resultList := make([]*github.Gist, 0)
	for _, gist := range gists {
		if _, ok := gistMap[*gist.ID]; !ok {
			resultList = append(resultList, gist)
		}
	}

	return resultList, nil
}

func (g *GistService) GetDetails(gist *github.Gist) (*domain.GistDetailsJson, error) {

	var detailsUrl string

	for k, v := range gist.Files {
		if k == "details.json" {
			detailsUrl = *v.RawURL
			break
		}
	}
	// Get deal json from gist

	if detailsUrl == "" {
		return nil, domain.ErrorGistDetailsJsonNotFound
	}

	// Parse url before passing it to GET - CWE-88
	// Call is only made to external systems, mostly done to silence SAST
	u, err := url.Parse(detailsUrl)

	if err != nil {
		return nil, errors.Errorf("%s", err)
	}

	// Pass URL
	resp, err := g.httpClient.Get(u.String())
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}

	if resp.StatusCode != 200 {
		return nil, errors.Errorf("%s", "Error getting details.json, status: "+resp.Status)
	}
	defer resp.Body.Close()

	// Populate deal object from response
	details := domain.GistDetailsJson{}

	if err := json.NewDecoder(resp.Body).Decode(&details); err != nil {
		return nil, errors.Errorf("Error decoding gist: %s", err)
	}

	return &details, nil

}

//List new gists since last visit
func (g *GistService) ListLatest(userID int, lastLogin time.Time) ([]*domain.Gist, error) {

	return g.repo.ListLatest(userID, lastLogin.Unix())
}
