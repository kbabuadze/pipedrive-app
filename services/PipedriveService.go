package services

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"gitlab.com/kbabuadze/pipedrive-app/domain"
)

type PipederiveService struct {
	client   *http.Client
	apiToken string
	baseURL  string
}

func NewPipedriveService(client http.Client, apiToken string, baseURL string) *PipederiveService {
	return &PipederiveService{
		client:   &client,
		apiToken: apiToken,
		baseURL:  baseURL,
	}
}

func (p *PipederiveService) ListOrganizations() ([]domain.PipedriveOrg, error) {
	resp, err := p.client.Get(p.baseURL + "/organizations/?api_token=" + p.apiToken)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	orgs := domain.PipedriveOrgResponse{}

	err = json.NewDecoder(resp.Body).Decode(&orgs)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}

	return orgs.Data, nil
}

// Create Organization in pipedrive if it does not exist
func (p *PipederiveService) CreateOrganization(name string) error {

	data := url.Values{}
	data.Set("name", name)

	body := strings.NewReader(data.Encode())

	r, err := http.NewRequest("POST", p.baseURL+"/organizations/?api_token="+p.apiToken, body) // URL-encoded payload
	if err != nil {
		return err
	}
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := p.client.Do(r)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 201 {
		return errors.Errorf("%s", "error adding org"+resp.Status)
	}
	return nil
}

func (p *PipederiveService) CreateUser(user domain.User) error {
	return nil
}

func (p *PipederiveService) ListUsers() error {
	return nil
}

func (p *PipederiveService) CreateDeal(deal *domain.PipedriveDeal) (*domain.PipedriveDealResponse, error) {

	// Prepare values for the Post
	data := url.Values{}
	data.Set("title", deal.Title)
	data.Set("value", fmt.Sprintf("%d", deal.Value))
	data.Set("currency", deal.Currency)

	// Url Encode values
	body := strings.NewReader(data.Encode())

	// Prepare request
	r, err := http.NewRequest("POST", p.baseURL+"/deals/?api_token="+p.apiToken, body) // URL-encoded payload
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	// Perform request
	resp, err := p.client.Do(r)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	defer resp.Body.Close()

	// Prepare Response
	dealResponse := domain.PipedriveDealResponse{}

	if err := json.NewDecoder(resp.Body).Decode(&dealResponse); err != nil {
		return nil, errors.Errorf("%s", err)
	}

	return &dealResponse, nil

}

func (p *PipederiveService) CreateActivity(activity *domain.PipedriveActivity, dealID int) (*domain.PipedriveActivityResponse, error) {
	// Prepare values for the Post
	data := url.Values{}
	data.Set("type", activity.Type)
	data.Set("deal_id", fmt.Sprintf("%d", dealID))

	// Url Encode values
	body := strings.NewReader(data.Encode())

	// Prepare request
	r, err := http.NewRequest("POST", p.baseURL+"/activities/?api_token="+p.apiToken, body) // URL-encoded payload
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	// Perform request
	resp, err := p.client.Do(r)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	defer resp.Body.Close()

	// Prepare Response
	activityResponse := domain.PipedriveActivityResponse{}

	if err := json.NewDecoder(resp.Body).Decode(&activityResponse); err != nil {
		return nil, errors.Errorf("%s", err)
	}

	return &activityResponse, nil
}

func (p *PipederiveService) CreateActivities(activities []domain.PipedriveActivity, dealID int) {
	for i := range activities {
		activity, err := p.CreateActivity(&activities[i], dealID)
		if err != nil {
			log.Logger.Error().Msgf("Error getting details.json: %v", err)
			continue
		}
		if activity.Success {
			log.Logger.Info().Msgf("Successfully created %v activity for the deail id: %v", activity.Data.Type, dealID)
		}
	}
}
