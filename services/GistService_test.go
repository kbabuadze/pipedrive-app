package services

import (
	"net/http"
	"testing"
	"time"

	"github.com/google/go-github/v40/github"
	"github.com/jarcoal/httpmock"
	"github.com/migueleliasweb/go-github-mock/src/mock"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
	"gitlab.com/kbabuadze/pipedrive-app/domain/mocks"
)

func TestListByID(t *testing.T) {
	mockGistRepo := new(mocks.MockGistRepository)
	mockedHTTPClient := mock.NewMockedHTTPClient(
		mock.WithRequestMatch(
			mock.GetUsersGistsByUsername,
			[]github.Gist{
				{
					ID: github.String("0000000000000000000000000000003"),
				},
				{
					ID: github.String("0000000000000000000000000000001"),
				},
			},
		),
	)

	c := github.NewClient(mockedHTTPClient)

	gistSvc := NewGistservice(mockGistRepo, c, &http.Client{})

	user := domain.User{
		ID:   1,
		Name: "john",
	}

	res, err := gistSvc.ListByID(&user)

	if err != nil {
		t.Errorf("%+v", err)
	}

	if len(res) > 1 {
		t.Errorf("Expected 1 result got %v", len(res))
	}
}

func TestGetDetails(t *testing.T) {

	// Prepare gist
	gist := github.Gist{ID: github.String("0000000000000000000000000000004"),

		Files: map[github.GistFilename]github.GistFile{
			"details.json": {
				RawURL: github.String("https://testgistrawurl"),
			},
		},
	}

	// Mock github.Client call
	mockGistRepo := new(mocks.MockGistRepository)
	mockedHTTPClientGithub := mock.NewMockedHTTPClient(
		mock.WithRequestMatch(
			mock.GetUsersGistsByUsername,
			[]github.Gist{
				gist,
			},
		),
	)

	// Mock http.Client call
	httpClient := &http.Client{Transport: &http.Transport{TLSHandshakeTimeout: 60 * time.Second}}
	httpmock.ActivateNonDefault(httpClient)

	httpmock.RegisterResponder("GET", "https://testgistrawurl",
		httpmock.NewStringResponder(200, `{
			"deal" : {
			  "title":"Deal",
			  "value": 100,
			  "currency":"EUR"
			},
			"activities":[
			  {
				"type":"call"
			  }
			]
		  }`,
		),
	)

	githubClient := github.NewClient(mockedHTTPClientGithub)

	gistSvc := NewGistservice(mockGistRepo, githubClient, httpClient)

	details, err := gistSvc.GetDetails(&gist)

	if err != nil {
		t.Errorf("%+v", err)
	}

	if details.Deal.Title != "Deal" {
		t.Errorf("Expected Deal as deal.title got %v", details.Deal.Title)
	}

}
