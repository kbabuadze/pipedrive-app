package services

import (
	"gitlab.com/kbabuadze/pipedrive-app/domain"
)

type SyncService struct {
	repo domain.SyncRepository
}

func NewSyncService(repo domain.SyncRepository) *SyncService {
	return &SyncService{
		repo: repo,
	}
}

func (ss *SyncService) GetFlag() (bool, error) {

	flag, err := ss.repo.GetFlag()
	if err != nil {
		return false, err
	}

	return flag.Processing, nil
}

func (ss *SyncService) SetFlag(sync *domain.Sync) error {
	return ss.repo.SetFlag(sync)
}
