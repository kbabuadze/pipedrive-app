package services

import (
	"github.com/pkg/errors"
	"gitlab.com/kbabuadze/pipedrive-app/delivery/api/dto"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
	"golang.org/x/crypto/bcrypt"
)

type UserService struct {
	repo domain.UsersRepositroy
}

// Create New instance of a UserService
func NewUsersService(repo domain.UsersRepositroy) *UserService {
	return &UserService{
		repo: repo,
	}
}

// Create User
func (u *UserService) Create(user *domain.User) error {
	return u.repo.Create(user)
}

// List all Users
func (u *UserService) ListAll() ([]*dto.User, error) {

	users, err := u.repo.ListAll()

	if err != nil {
		return nil, errors.Errorf("%s", err)
	}

	usersResult := make([]*dto.User, 0)

	for i := range users {

		lastLogin := users[i].LastLogin.Time.String()
		if users[i].LastLogin.Time.IsZero() {
			lastLogin = "Never"
		}

		lastScan := users[i].LastScan.Time.String()
		if users[i].LastScan.Time.IsZero() {
			lastScan = "Never"
		}
		usersResult = append(usersResult, &dto.User{
			ID:        users[i].ID,
			Name:      users[i].Name,
			LastLogin: lastLogin,
			LastScan:  lastScan,
		})
	}

	return usersResult, nil

}

// Find user and compare bcrypt hash and pass
func (u *UserService) Login(username, password string) (*domain.User, error) {

	user, err := u.repo.GetByName(username)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return nil, err
	}
	return user, nil

}

func (u *UserService) Update(user *domain.User) error {
	return u.repo.UpdateLastLogin(user)
}

func (u *UserService) UpdateLastScan(user *domain.User) error {
	return u.repo.UpdateLastScan(user)
}

func (u *UserService) GetByName(username string) (*domain.User, error) {
	return u.repo.GetByName(username)
}
