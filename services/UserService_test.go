package services

import (
	"testing"

	"gitlab.com/kbabuadze/pipedrive-app/domain/mocks"
)

func TestListAll(t *testing.T) {
	mockUserRepo := new(mocks.MockUserRepository)
	userSvc := NewUsersService(mockUserRepo)
	users, err := userSvc.ListAll()

	if err != nil {
		t.Error(err.Error())
	}

	// User0

	timeUser0 := "Never"

	if len(users) == 0 {
		t.Errorf("Recieved 0 users")
	}

	if users[0].ID != 1 {
		t.Errorf("Expected ID %v got %v", 1, users[0].ID)
	}

	if users[0].Name != "john" {
		t.Errorf("Expected user %v got %v", "john", users[0].Name)
	}

	if users[0].LastLogin != "Never" {
		t.Errorf("Expected Lastlogin:  %v got %v", timeUser0, users[0].LastLogin)
	}

	if users[0].LastScan != "Never" {
		t.Errorf("Expected LasScan: %v got %v", timeUser0, users[0].LastScan)
	}

	// User1

	timeUser1 := "2012-12-21 00:00:01 +0000 UTC"
	if users[1].ID != 1 {
		t.Errorf("Expected ID %v got %v", 1, users[1].ID)
	}

	if users[1].Name != "paul" {
		t.Errorf("Expected user %v got %v", "john", users[1].Name)
	}

	if users[1].LastLogin != timeUser1 {
		t.Errorf("Expected Lastlogin:  %v got %v", timeUser1, users[1].LastLogin)
	}

	if users[1].LastScan != timeUser1 {
		t.Errorf("Expected LasScan: %v got %v", timeUser1, users[1].LastScan)
	}
}
