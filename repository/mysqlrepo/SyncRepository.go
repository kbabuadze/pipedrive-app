package mysqlrepo

import (
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
)

type SyncRepoMysql struct {
	db *sqlx.DB
}

func NewMysqlSyncRepo(db *sqlx.DB) domain.SyncRepository {
	return SyncRepoMysql{
		db: db,
	}
}

func (sr SyncRepoMysql) SetFlag(sync *domain.Sync) error {

	_, err := sr.db.NamedExec(`UPDATE sync SET processing = :processing`, sync)
	if err != nil {
		return errors.Errorf("%s", err)
	}

	return nil
}

func (sr SyncRepoMysql) GetFlag() (*domain.Sync, error) {
	var sync domain.Sync

	err := sr.db.Get(&sync, "SELECT processing FROM sync LIMIT 1")
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	return &sync, nil
}
