package mysqlrepo

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
)

type UsersRepoMysql struct {
	db *sqlx.DB
}

func NewMysqlUserRepo(db *sqlx.DB) domain.UsersRepositroy {
	return &UsersRepoMysql{
		db: db,
	}
}

func (m *UsersRepoMysql) GetByID(id int) (*domain.User, error) {
	return &domain.User{}, nil
}

// Create user
func (m *UsersRepoMysql) Create(user *domain.User) error {
	_, err := m.db.NamedExec(`INSERT INTO users (name,password) VALUES (:name,:password)`, user)
	if err != nil {
		return errors.Errorf("%s", err)
	}
	return err
}

// List all users
func (m *UsersRepoMysql) ListAll() ([]domain.User, error) {
	users := []domain.User{}

	err := m.db.Select(&users, "SELECT id,name,last_login,last_scan,UNIX_TIMESTAMP(created_at) as created_at FROM users")
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	return users, nil
}

// Get user by username and password
func (m *UsersRepoMysql) GetByName(username string) (*domain.User, error) {

	user := domain.User{}
	err := m.db.Get(&user, "SELECT id,name,password,last_login,last_scan FROM users WHERE name =?  LIMIT 1", username)
	if err == sql.ErrNoRows {
		user.ID = -1
		return &user, nil
	}
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	return &user, nil
}

// Update users last login
func (m *UsersRepoMysql) UpdateLastLogin(user *domain.User) error {

	_, err := m.db.NamedExec(`UPDATE users SET last_login = CURRENT_TIMESTAMP WHERE id = :id`, user)
	if err != nil {
		return errors.Errorf("%s", err)
	}

	return nil
}

// Update users last_scan
func (m *UsersRepoMysql) UpdateLastScan(user *domain.User) error {
	_, err := m.db.NamedExec(`UPDATE users SET last_scan = CURRENT_TIMESTAMP WHERE id = :id`, user)
	if err != nil {
		return errors.Errorf("%s", err)
	}

	return nil
}
