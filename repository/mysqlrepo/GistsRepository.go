package mysqlrepo

import (
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
)

type GistsRepoMysql struct {
	db *sqlx.DB
}

func NewMysqlGistsRepo(db *sqlx.DB) domain.GistsRepository {
	return &GistsRepoMysql{
		db: db,
	}
}

// Create creates gist entry in MySQL
func (m *GistsRepoMysql) Create(userID int, gist domain.Gist) error {
	_, err := m.db.NamedExec(`INSERT INTO gists (user_id,id) VALUES (:user_id,:id) ON DUPLICATE KEY UPDATE user_id = user_id`, map[string]interface{}{
		"user_id": userID,
		"id":      gist.ID,
	})

	if err != nil {
		return errors.Errorf("%s", err)
	}
	return nil
}

// ListByID lists all the gists with specified userID
func (m *GistsRepoMysql) ListByUserID(userID int) ([]*domain.Gist, error) {
	gists := []*domain.Gist{}

	err := m.db.Select(&gists, `SELECT user_id,id,UNIX_TIMESTAMP(created_at) as created_at FROM gists WHERE user_id=?`, userID)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	return gists, nil
}

// ListLates gists by userID and lastLogin
func (m *GistsRepoMysql) ListLatest(userID int, lastLogin int64) ([]*domain.Gist, error) {
	gists := []*domain.Gist{}

	err := m.db.Select(&gists, `SELECT user_id,id,UNIX_TIMESTAMP(created_at) as created_at FROM gists WHERE user_id=? AND UNIX_TIMESTAMP(created_at) > ?`, userID, lastLogin)
	if err != nil {
		return nil, errors.Errorf("%s", err)
	}
	return gists, nil
}
