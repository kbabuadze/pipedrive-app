package mysqlrepo

import (
	"log"
	"testing"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kbabuadze/pipedrive-app/domain"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func NewMock() (*sqlx.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	sqlxDB := sqlx.NewDb(db, "sqlmock")
	return sqlxDB, mock
}

var gist = &domain.Gist{
	ID:     uuid.New().String(),
	UserID: 1,
}

func TestListByID(t *testing.T) {
	db, mock := NewMock()
	repo := NewMysqlGistsRepo(db)

	query := `SELECT user_id,id,UNIX_TIMESTAMP\(created_at\) as created_at FROM gists WHERE user_id=?`

	rows := sqlmock.NewRows([]string{"user_id", "id"}).
		AddRow(gist.UserID, gist.ID)

	mock.ExpectQuery(query).WithArgs(gist.UserID).WillReturnRows(rows)

	gists, err := repo.ListByUserID(gist.UserID)
	assert.NotNil(t, gists)
	assert.NoError(t, err)
}

func TestCreateGist(t *testing.T) {
	db, mock := NewMock()
	repo := NewMysqlGistsRepo(db)

	query := `INSERT INTO gists \(user_id,id\) VALUES \(\?,\?\) ON DUPLICATE KEY UPDATE user_id = user_id`

	mock.ExpectExec(query).WithArgs(gist.UserID, gist.ID).WillReturnResult(sqlmock.NewResult(0, 1))

	err := repo.Create(1, *gist)
	assert.NoError(t, err)
}
