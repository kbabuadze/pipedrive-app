module gitlab.com/kbabuadze/pipedrive-app

go 1.16

require (
	github.com/gin-contrib/logger v0.2.0
	github.com/gin-contrib/sessions v0.0.4
	github.com/gin-gonic/gin v1.7.4
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/go-github/v40 v40.0.0
	github.com/google/uuid v1.3.0
	github.com/jarcoal/httpmock v1.0.8
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.4.0
	github.com/migueleliasweb/go-github-mock v0.0.5
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.11.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/rs/zerolog v1.26.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)
