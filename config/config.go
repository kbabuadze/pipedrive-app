package config

import (
	"os"

	"github.com/joho/godotenv"
)

type PipeConfig struct {
	MySQLParams
	GitHubParams
	PipedriveParams
	CronParams
	MetricsParams
	RedisParams
	SessionParams
}

// Github API params
type GitHubParams struct {
	ApiToken string
}

// Pipedrive API params
type PipedriveParams struct {
	ApiURL   string
	ApiToken string
}

// Mysql connection params
type MySQLParams struct {
	Username string
	Password string
	Host     string
	Port     string
	Database string
}

// Cron params
type CronParams struct {
	CronInterval string
}

// Metrics params
type MetricsParams struct {
	Uername  string
	Password string
}

// Redis params
type RedisParams struct {
	Address  string
	Password string
	Key      string
}

// Session params
type SessionParams struct {
	Domain string
}

func NewConfig() *PipeConfig {
	// Setup .env
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}

	config := &PipeConfig{
		MySQLParams: MySQLParams{
			Username: os.Getenv("MYSQL_USERNAME"),
			Password: os.Getenv("MYSQL_PASSWORD"),
			Host:     os.Getenv("MYSQL_HOST"),
			Port:     os.Getenv("MYSQL_PORT"),
			Database: os.Getenv("MYSQL_DATABASE"),
		},
		GitHubParams: GitHubParams{
			ApiToken: os.Getenv("GIT_API_TOKEN"),
		},
		PipedriveParams: PipedriveParams{
			ApiURL:   os.Getenv("PIPEDRIVE_API_URL"),
			ApiToken: os.Getenv("PIPEDRIVE_API_TOKEN"),
		},
		CronParams: CronParams{
			CronInterval: os.Getenv("CRON_INTERVAL"),
		},
		MetricsParams: MetricsParams{
			Uername:  os.Getenv("METRICS_AUTH_USER"),
			Password: os.Getenv("METRICS_AUTH_PASS"),
		},
		RedisParams: RedisParams{
			Address:  os.Getenv("REDIS_ADDRESS"),
			Password: os.Getenv("REDIS_PASSWORD"),
			Key:      os.Getenv("REDIS_KEY"),
		},
		SessionParams: SessionParams{
			Domain: os.Getenv("SESSION_DOMAIN"),
		},
	}

	return config
}
